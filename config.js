
// Devices https://github.com/GoogleChrome/puppeteer/blob/master/lib/DeviceDescriptors.js

let NETWORK_PRESETS = {
  'GPRS': {
    'offline': false,
    'downloadThroughput': 50 * 1024 / 8,
    'uploadThroughput': 20 * 1024 / 8,
    'latency': 500
  },
  'Regular2G': {
    'offline': false,
    'downloadThroughput': 250 * 1024 / 8,
    'uploadThroughput': 50 * 1024 / 8,
    'latency': 300
  },
  'Good2G': {
    'offline': false,
    'downloadThroughput': 450 * 1024 / 8,
    'uploadThroughput': 150 * 1024 / 8,
    'latency': 150
  },
  'Regular3G': {
    'offline': false,
    'downloadThroughput': 750 * 1024 / 8,
    'uploadThroughput': 250 * 1024 / 8,
    'latency': 100
  },
  'Good3G': {
    'offline': false,
    'downloadThroughput': 1.5 * 1024 * 1024 / 8,
    'uploadThroughput': 750 * 1024 / 8,
    'latency': 40
  },
  'Regular4G': {
    'offline': false,
    'downloadThroughput': 4 * 1024 * 1024 / 8,
    'uploadThroughput': 3 * 1024 * 1024 / 8,
    'latency': 20
  },
  'DSL': {
    'offline': false,
    'downloadThroughput': 2 * 1024 * 1024 / 8,
    'uploadThroughput': 1 * 1024 * 1024 / 8,
    'latency': 5
  },
  'WiFi': {
    'offline': false,
    'downloadThroughput': 30 * 1024 * 1024 / 8,
    'uploadThroughput': 15 * 1024 * 1024 / 8,
    'latency': 2
  }
}

const tests = [
  {
    reportName: 'Home_to_marketplace-desktop',
    entryUrl: 'https://trademe.nz',
    entryWaitTime: 5000,
    steps: [
      {
        waitTime: 2000,
        actionSelector: '.homepage__vertical-links-link--marketplace'
      }
    ]
  },
  {
    reportName: 'Home_to_marketplace-mobile',
    device: 'Pixel 2',
    newtworkConfig: NETWORK_PRESETS['Good3G'],
    entryUrl: 'https://trademe.nz',
    entryWaitTime: 5000,
    steps: [
      {
        waitTime: 2000,
        actionSelector: '.homepage__vertical-links-small-link--marketplace'
      }
    ]
  },
  {
    reportName: 'Home_to_marketplace-mobile-slow-network',
    device: 'Pixel 2',
    newtworkConfig: NETWORK_PRESETS['Regular2G'],
    entryUrl: 'https://trademe.nz',
    entryWaitTime: 5000,
    steps: [
      {
        waitTime: 2000,
        actionSelector: '.homepage__vertical-links-small-link--marketplace'
      }
    ]
  }
];

module.exports = tests; 