const puppeteer = require('puppeteer');
const devices = require('puppeteer/DeviceDescriptors');
const fs = require('fs');
const { join } = require('path');

const tests = require('./config');

const REPORT_DIR = 'reports';


(async () => {
  const browser = await puppeteer.launch();

  const page = await browser.newPage();

  await new Promise(resolve => fs.mkdir(join(__dirname, REPORT_DIR), resolve));

  for await (const test of tests) {
    await new Promise(resolve => fs.mkdir(join(__dirname, REPORT_DIR, test.reportName), resolve));

    if (test.device) {
      await page.emulate(devices[test.device]);
    }

    if (test.newtworkConfig) {
      const client = await page.target().createCDPSession();
      await client.send('Network.emulateNetworkConditions', test.newtworkConfig);
    }

    await page.goto(test.entryUrl);

    await page.screenshot({ path: join(REPORT_DIR, test.reportName, 'screenshot-01.png') });

    // TODO find better way to wait until page is loaded
    await new Promise(resolve => setTimeout(resolve, test.entryWaitTime || 0));

    await page.screenshot({ path: join(REPORT_DIR, test.reportName, 'screenshot-02.png') });

    await page.waitForSelector('title');
    // TODO wait a set amount of time

    await page.tracing.start({
      path: join(REPORT_DIR, test.reportName, 'trace.json'),
      screenshots: true
    });

    for await (const [index, step] of test.steps.entries()) {
      const button = await page.$(step.actionSelector);

      await button.click();

      await page.screenshot({ path: join(REPORT_DIR, test.reportName, `step-${index}-screenshot-02.png`) });

      await page.waitForSelector('title');

      await page.screenshot({ path: join(REPORT_DIR, test.reportName, `step-${index}-screenshot-03.png`) });

      // TODO find better way to wait until page is loaded
      await new Promise(resolve => setTimeout(resolve, step.waitTime));

      await page.screenshot({ path: join(REPORT_DIR, test.reportName, `step-${index}-screenshot-04.png`) });
    }

    await page.tracing.stop();
  }

  await browser.close();
})();